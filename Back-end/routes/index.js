var express = require("express")
var router = express.Router();


router.use('/shop', require('./shop'));

/* GET home page. */
router.use('/', (req, res) => {
    res.send('Root Backend')
});

module.exports = router;
