const express = require('express')
const { ProductController } = require('../controllers/shop')
const router = express.Router()

router.get('/getAllProducts', ProductController.List)
router.get('/products/:id', ProductController.Get)

module.exports = router
