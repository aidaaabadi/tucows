const path = require('path')
const express = require('express')
const app = express()
const cors = require("cors");

app.use(cors());
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))
app.use("/", require("./routes/index"));

app.use((req, res, next) => {
  res.status(404).send('<h2>Not Found.</h2>')
})
const PORT = 8000
app.listen(PORT, () => {
  console.log(`REST server started on ${PORT}`);
})
