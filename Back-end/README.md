# Express.js Project README

## Getting Started

### Installation
To install the necessary dependencies for the Express.js project, run the following commands:
### `npm install`


### Running
### `npm start`
Start the backend server by navigating to [http://localhost:8000](http://localhost:8000) in your web browser. Confirm that you receive the "Root Backend" text to verify the backend is functioning correctly.


