"use strict";
const joi = require("joi");
const base = require("../../models/products.json");

class ProductController {

  static async List(req, res) {
    const Schema = joi.object().keys({
      page: joi.number().min(1).default(1),
      limit: joi.number().min(1).max(20).default(10),
      name: joi.string(),
    });
    const { error, value } = Schema.validate(req.query, { abortEarly: true });
    if (error) {
      return res.status(400).json({ message: 'Invalid Data' });
    }
    let products = [...base]
    if (value.name)
      products = products.filter(p => p.name.toLowerCase().includes(value.name.toLowerCase()));

    const totalCount = products.length
    products = products.slice((value.page - 1) * value.limit, value.page * value.limit)
    return res.status(200).json({ page: value.page, limit: value.limit, totalCount, data: products });
  }

  static async Get(req, res) {
    const Schema = joi.object().keys({
      id: joi.number().min(1),
    });
    const { error, value } = Schema.validate(req.params, { abortEarly: true });
    if (error) {
      return res.status(400).json({ message: 'Invalid Data' });
    }
    const products = base.find(el => el.id === value.id)
    if (!products)
      return res.status(404).json({ message: 'Product not found.' });

    return res.status(200).json({ products });
  }

}

module.exports = ProductController;
