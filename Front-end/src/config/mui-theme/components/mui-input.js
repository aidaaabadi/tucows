export default {
	MuiInputLabel: {
		styleOverrides: {
			root: ({ theme }) => ({
				backgroundColor: "#fff",
				// height: "44px",
				// color: theme.palette.gray["400"],
				fontSize: "14px",
				// fontWeight: 400,
				transform: "translate(20px,11px) scale(1)",
				padding: "0 4px",
				borderRadius: "14px",
				"&.Mui-disabled": {
					color: theme.palette.secondary["400"],
				},
				// "&.Mui-focused": {
				// 	color: theme.palette.gray["400"],
				// },
			}),
			shrink: ({ theme }) => ({
				transform: "translate(16px,-8px) scale(1)",
				fontSize: "12px",
				color: theme.palette.secondary["400"],
			}),
			// focused: ({ theme }) => ({
			// 	color: theme.palette.gray["400"],
			// }),
			// filled: {
			// 	transform: "translate(14px,-13px) scale(1)",
			// 	fontSize: "12px",
			// },
		},
	},
	MuiFormControl: {
		root: {
			height: "44px",
		},
	},
	MuiOutlinedInput: {
		styleOverrides: {
			root: ({ theme }) => ({
				borderRadius: "8px",
				height: "44px",
				backgroundColor: "#fff",
				"&.Mui-disabled": {
					"& .MuiOutlinedInput-notchedOutline": {
						borderColor: `${theme.palette.secondary["300"]}`,
					},
				},
			}),
			input: {
				padding: "0 24px",
				fontSize: "14px",
			}, 
			notchedOutline: ({ theme }) => ({
				padding: "0 14px",
				borderWidth: "1px !important",
				borderColor: `${theme.palette.secondary["400"]}`,
				// 			top: 0
			}),
		},
	},
	// MuiFormHelperText: {
	// 	styleOverrides: {
	// 		root: ({ theme }) => ({
	// 			display: "flex",
	// 			alignItems: "center",
	// 			gap: "2px",
	// 			margin: "3px 0 0 0",

	// 			"& svg": {
	// 				width: "18px",
	// 				height: "18px"
	// 			},
	// 			span: {
	// 				fontSize: "12px !important",
	// 			},
	// 		}),
	// 	},
	// },
};
