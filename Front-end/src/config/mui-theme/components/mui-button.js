export default {
	MuiButton: {
		defaultProps: {
			disableElevation: true,
			disableRipple: true,
			disableFocusRipple: true,
		},
		styleOverrides: {
			root: ({ theme }) => ({
				outline: "none",
				borderRadius: "6px",
				fontWeight: 400,
				"&.Mui-disabled": {
					backgroundColor: theme.palette.secondary["300"],
					border: "none",
					color: theme.palette.secondary["400"],
				},
				// "&.icon": {
				// 	minWidth: "40px !important",
				// 	padding: "0 !important",
				// },
			}),
			containedPrimary: ({ theme }) => containedButtonGenerator(theme, "primary"),
			containedError: ({ theme }) => containedButtonGenerator(theme, "error"),
			containedWarning: ({ theme }) => containedButtonGenerator(theme, "warning"),
			outlinedPrimary: ({ theme }) => outlinedButtonGenerator(theme, "primary"),
			outlinedError: ({ theme }) => outlinedButtonGenerator(theme, "error"),
			outlinedWarning: ({ theme }) => outlinedButtonGenerator(theme, "warning"),
			outlinedSecondary: ({ theme }) => outlinedButtonGenerator(theme, "secondary"),

			sizeXs: {
				minWidth: "80px",
				height: "32px",
				borderWidth: "1px !important",
			},
			sizeSmall: {
				minWidth: "auto",
				height: "32px",
				borderWidth: "1px !important",
			},
			sizeMedium: {
				minWidth: "220px",
				height: "38px",
			},
			sizeLarge: {
				minWidth: "350px",
				height: "38px",
			},
		},
	},
};

function outlinedButtonGenerator(theme, color) {
	return {
		border: `2px solid ${theme.palette[color].main}`,
		color: theme.palette[color].main,
		"&.Mui-disabled": {
			backgroundColor: "transparent",
			border: `2px solid ${theme.palette.secondary["400"]}`,
			color: theme.palette.secondary["400"],
		},
		"&:hover": {
			border: `2px solid ${theme.palette[color]["600"]}`,
			backgroundColor: theme.palette[color]["100"],
		},
		"&:focus": {
			backgroundColor: `${theme.palette[color]["200"]} !important`,
		},
	};
}
function containedButtonGenerator(theme, color) {
	return {
		border: `3px solid ${theme.palette[color].main}`,
		"&:focus": {
			backgroundColor: `${theme.palette[color].main} !important`,
			borderColor: theme.palette[color]["600"],
		},
		"&:hover": {
			borderColor: theme.palette[color]["600"],
			backgroundColor: theme.palette[color]["600"],
		},
	};
}
