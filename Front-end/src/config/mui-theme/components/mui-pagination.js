export default {
	MuiPaginationItem: {
		styleOverrides: {
			root: ({ theme }) => ({
				borderRadius: "6px",
				backgroundColor: `rgba(237, 237, 254, 1)`,
				minWidth: "30px", 
				height: "30px",

				"&.Mui-selected": {
					backgroundColor: `rgba(96, 93, 236, 1)`,
					color: "#fff",
					"&:hover": {
						backgroundColor: `rgba(96, 93, 236, 1)`,
						color: "#fff",
					},
				},
				"&.Mui-disabled": {
					"& svg": {
						color: `rgba(237, 237, 254, 1)`,
					},
				},
				"&:hover": {
					backgroundColor: `rgba(96, 93, 236, 1)`,
					color: "#fff",
					"& svg": {
						color: `#fff`,
					},
				},
			}),
			icon: ({ theme }) => ({
				color: `rgba(96, 93, 236, 1)`,
			}),
		},
	},
};
