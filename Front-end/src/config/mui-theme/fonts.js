import nunitiRegular from "@/assets/fonts/NunitoSans/NunitoSans_7pt-Regular.woff2"
import nunitittfRegular from "@/assets/fonts/NunitoSans/NunitoSans_7pt-Regular.ttf"
import nunitiBold from "@/assets/fonts/NunitoSans/NunitoSans_7pt-Bold.woff2"
import nunitittfBold from "@/assets/fonts/NunitoSans/NunitoSans_7pt-Bold.ttf"
import interFont from "@/assets/fonts/Inter/Inter-Regular.woff2"
import interFontBold from "@/assets/fonts/Inter/Inter-Bold.woff2"
import interFontttf from "@/assets/fonts/Inter/Inter-Regular.ttf"

export default {
    MuiCssBaseline: {
        styleOverrides: `
                @font-face {
                font-family: nunitiSans;
                font-style: normal;
                font-weight: 400; 
                src:  url(${nunitiRegular}) format('woff2');
                src:  url(${nunitittfRegular}) format('ttf');
            }
            @font-face {
                font-family: nunitiSans;
                font-style: bold;
                font-weight: 700; 
                src:  url(${nunitiBold}) format('woff2');
                src:  url(${nunitittfBold}) format('ttf');
            }      
                @font-face {
                font-family: interFont;
                font-style: normal;
                font-weight: 400; 
                src:  url(${interFont}) format('woff2');
                src:  url(${interFontttf}) format('ttf');
            } 
                     }      
                @font-face {
                font-family: interFont;
                font-style: bold;
                font-weight: 700; 
                src:  url(${interFontBold}) format('woff2');
                src:  url(${interFontBold}) format('ttf');
            } 
            
            
      `,
    },
}