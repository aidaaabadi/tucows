import { createTheme } from "@mui/material/styles";

import muiButton from "./components/mui-button";
import muiInput from "./components/mui-input";
import muiTabelCell from "./components/mui-tabelCell";
import muiPagination from "./components/mui-pagination";


import colors from "./colors";
import fonts from "./fonts";

export const muiTheme = createTheme({
	direction: "ltr",
	textTransform:"none",
	typography: {
		fontFamily: "nunitiSans",
		// fontFamily: 'Nunito Sans',
		fontWeightRegular: 400,
	},
	palette: {
		...colors,
	},
	components: {
		...fonts,
		...muiButton,
		...muiInput,
		...muiTabelCell,
		...muiPagination,
	},
	breakpoints: {
		values: {
			xs: 0,
			sm: 680,
			md: 960,
			lg: 1280,
			xl: 1920,
		},
	},
});
