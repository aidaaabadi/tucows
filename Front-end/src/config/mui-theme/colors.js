export default {
	primary: {
		main: "rgba(96, 93, 236, 1)",
		100: "rgba(237, 237, 254, 1)",
		contrastText: "#fff",
	},
 green:{
	 main: "rgba(49, 170, 39, 1)",
	 100: "rgba(235, 246, 235, 1)",

 },

	orange:{
	 main: "rgba(233, 149, 24, 1)",
	 100: "rgba(255, 244, 228, 1)",

 },
	red:{
	 main: "rgba(249, 50, 50, 1)",
	 100: "rgba(255, 240, 240, 1)",

 },


	error: {
		main: "#DD5835",
		100: "#F8D9C7",
		200: "#F2B69B",
		300: "#EA9775",
		400: "#E47953",
		500: "#DD5835",
		600: "#91282A",
		contrastText: "#fff",
	},
	warning: {
		main: "#FDB940",
		100: "#FEF0D5",
		200: "#FFE0AF",
		300: "#FED28B",
		400: "#FFC768",
		500: "#FDB940",
		600: "#F68D2E",

		contrastText: "#4F4F4F",
	},
	secondary: {
		main: "rgba(26, 26, 26, 1)",
		100: "rgba(249, 249, 251, 1)",
		200: "rgba(0, 0, 0, 0.3)",
		300: "rgba(128, 128, 128, 1)",
		400: "rgba(112, 112, 112, 1)",
		500: "#000000",
		600: "rgba(220, 223, 227, 1)",
		contrastText: "rgba(255, 255, 255, 0.87)",
	},
};
