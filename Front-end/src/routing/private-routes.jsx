import { lazy, Suspense } from "react";
import { MasterLayout } from "@/features/layout/master-layout";
import { Box, CircularProgress } from "@mui/material";
const Products = lazy(() => import("@/screens/products"));
const SuspensedView = ({ element }) => {
	return (
		<Suspense
			fallback={
				<Box
					sx={{
						position: "fixed",
						top: "0",
						right: "0",
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
						width: "100%",
						height: "100%",
					}}
				>
					<CircularProgress />
				</Box>
			}
		>
			{element}
		</Suspense>
	);
};
const PrivateRoutes = [
	{
		path: "/",
		element: <MasterLayout />,
		children: [
			{
				path: "/",
				element:<Products/>,
				children: [

					{
						path: "/products",
						element: <SuspensedView element={<Products/>} />,
					}
				],
			},
		],
	},
];

export { PrivateRoutes };
