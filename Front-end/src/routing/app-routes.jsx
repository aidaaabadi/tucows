import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { PrivateRoutes } from "./private-routes";
import { App } from "../App";

function AppRoutes() {
	const router = createBrowserRouter([
		{
			path: "/",
			element: <App />,
			children: PrivateRoutes,
		},
	]);

	return <RouterProvider router={router} />;
}

export { AppRoutes };
