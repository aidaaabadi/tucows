import { useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { Grid, TableBody, Typography, useMediaQuery } from "@mui/material";
import useSWR from "swr";
import { useTheme } from "@emotion/react";
// Utils
import { productsService } from "@/service/api/products.service";
import { makeQueryString } from "@/utils";

// Components
import { ProductListItem } from "@/features/products/index";
import { ITable } from "@/features/ui";
import { ILoadingContainer } from "@/features/ui/i-loading-container";
import { IPagination } from "@/features/ui/i-pagination";
import { ISpinner } from "@/features/ui/i-spinner";
import { ProductDetail } from "@/features/product-detail";



const tableColumns = [{ title: "ID" }, { title: "Status" }, { title: "Quantity" }, { title: "Product Name" }, { title: "Price" }];
const tableColumnsMobile = [{ title: "Product Name" }];

export default function Products() {
    const navigate = useNavigate();
    const [searchParams] = useSearchParams();
    const theme = useTheme();
    const [selectedProductForShow, setSelectedProductForShow] = useState(null);
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));


    // we use this object to have the latest selected filters for api
    const usp = {
        page: searchParams.get("page") || 1,
        limit: searchParams.get("limit") || 10,
        name: searchParams.get('search') || ""
    };

    const {
        data: products,
        mutate,
        isValidating,
        isLoading,
    } = useSWR(`products?${makeQueryString(usp)}`, () => productsService.getProducts(makeQueryString(usp)));

    const pageChangedHandler = (event, value) => {
        navigate(`?${makeQueryString({ page: value })}`);
    };
    return (
        <>
            <Grid container>

                {isLoading && !products && <ISpinner />}
                {products && (
                    <>
                        <Grid item xs={12} display="flex">
                            <ILoadingContainer loading={isValidating}>
                                {products && (
                                    <>

                                        <Grid item xs={12}>
                                            <Typography mb={"5px"}><strong>Products </strong><span style={{
                                                fontSize: 12,
                                                color: "rgba(128, 128, 128, 1)",
                                                marginLeft: 10
                                            }}>{`${products.totalCount > products.limit ? products.limit * products.page : products.totalCount} of ${products.totalCount} results`}</span>
                                            </Typography>

                                            <ITable color="primary">
                                                <ITable.Head columns={isMobile ? tableColumnsMobile : tableColumns}  isMobile={isMobile}/>
                                                <TableBody>
                                                    { products?.data && products.data.length>0 ?
                                                        products?.data.map((product) => (
                                                            <ProductListItem
                                                                key={product.id}
                                                                product={product}
                                                                multiSelection={false}  //for having multi select product multiSelection should be true
                                                                onShow={() => {
                                                                    setSelectedProductForShow(product);
                                                                }}
                                                            />
                                                        ))
                                                    :
                                                        <Typography m={"10px"} textAlign={'center'}>No data to display</Typography>
                                                    }
                                                </TableBody>
                                            </ITable>
                                        </Grid>
                                        <IPagination page={+products.page} count={products.totalCount} limit={+products.limit}
                                            onChange={pageChangedHandler} />
                                    </>
                                )}
                            </ILoadingContainer>
                        </Grid>
                    </>
                )}
            </Grid>



            <ProductDetail
                isMobile={isMobile}
                open={!!selectedProductForShow}
                product={selectedProductForShow}
                onClose={() => setSelectedProductForShow(null)}
            />

        </>
    );
}
