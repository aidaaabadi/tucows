import axios from "axios";
let headers = {
	"Accept-Language": "en"
};
export default axios.create({
	baseURL: import.meta.env.VITE_API_URL,
	headers,
});
