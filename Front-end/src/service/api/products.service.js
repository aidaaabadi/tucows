import useFetch from "../index";

class products {
	async getProducts(params) {
		const res = await useFetch.get(`/shop/getAllProducts`, { params });
		return res.data;
	}

	async getProduct(productId) {
		const res = await useFetch.get(`/shop/products/${productId}`);
		return res.data;
	}
}
const productsService = new products();
export { productsService };
