import { Outlet } from "react-router-dom";
import { Box } from "@mui/material";

function App() {
	return (
		<Box sx={{ minHeight: "100%", width: "100%", display: "flex" }}>
				<Outlet />
		</Box>
	);
}

export { App };
