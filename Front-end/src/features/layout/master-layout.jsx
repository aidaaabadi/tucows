import {Outlet} from "react-router-dom";
import {Box, Grid} from "@mui/material";
import {LayoutTabs} from "./tabs";



function MasterLayout() {
    return (
        <Box sx={masterLayoutStyles}>
            <Box bgcolor="#FFFFFF" sx={contentStyles}>
                <LayoutTabs/>
                <Grid container>
                    <Outlet/>
                </Grid>
            </Box>
        </Box>
    );
}

const masterLayoutStyles = {
    minHeight: "100%",
    width: "85%",
    margin: "0 auto",
    display: "flex",
    position: "relative",
};
const contentStyles = {
    minHeight: "100%",
    width: "100%",
    borderRadius: "50px 0px 0px 50px",
    p: "50px 0",
    position: "relative",
};
export {MasterLayout};
