import {Box, Button, Drawer, Grid, Hidden, IconButton} from "@mui/material";
import {Link, useNavigate} from "react-router-dom";
import {useState} from "react";
// Assets
import {StyledBox} from "./tabs.styles";
import {AccountIcon, NotifIcon, SettingIcon, StormLogo} from "@/assets/icons";
import * as PropTypes from "prop-types";
import MenuIcon from '@mui/icons-material/Menu';
import {IInput} from "@/features/ui";

function DrawerHeader(props) {
    return null;
}

DrawerHeader.propTypes = {children: PropTypes.node};

function LayoutTabs() {
    const navigate = useNavigate();
    const [open, setOpen] = useState(false);
    const [inputValue, setInputValue] = useState("");
    const [error, setError] = useState("");

    const tabs = [
        {
            icon: <SettingIcon/>,
            path: "/products",
        },
        {
            icon: <NotifIcon/>,
            path: "/products",
        },
        {
            icon: <AccountIcon/>,
            path: "/products",
            title: 'Adriana Arias'
        },
    ];

    const toggleDrawer = (newOpen) => {
        setOpen(newOpen)
    }

    const filterBySearch = () => {
        let temp = {}
        temp.search = inputValue;
        navigate(`?${new URLSearchParams(temp)}`)
    }

    const inputChangeHandler = (e) => {
        const inputValue = e.target.value;
        // Filter out invalid characters
        const filteredValue = inputValue.replace(/[^a-zA-Z0-9 ]/g, '');
        // Check if the filtered value is different from the original input
        if (filteredValue !== inputValue) {
            setError('Invalid characters entered');
        } else {
            setError('');
        }


        setInputValue(e.target.value);

    };
    return (
        <Box>
            <Grid container alignItems={'center'} mb={'50px'}>
                <Grid item xs={6}  sm={12}  md={6} alignSelf={'center'}>
                    <StormLogo style={logoStyles}/>
                </Grid>
                <Grid item xs={6} sm={12}  md={6}  textAlign={"right"}>

                    <Hidden smUp>

                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            edge="end"
                            onClick={toggleDrawer}
                        >
                            <MenuIcon/>
                        </IconButton>


                            <Drawer

                                sx={{
                                    width: 100,
                                    flexShrink: 0,
                                    '& .MuiDrawer-paper': {
                                        width: 100,
                                    },
                                }}

                                anchor="right"
                                open={open}
                                onClose={()=>toggleDrawer(false)}
                            >

                            <StyledBox>
                                <nav>
                                    <ul style={ulMenuMobile}>
                                        {tabs.map((tab, index) => (
                                            <li key={`layout-tab-${index}`}>
                                                <Link to={tab.path}>{tab.icon}</Link>
                                            </li>
                                        ))}
                                    </ul>
                                </nav>
                            </StyledBox>


                        </Drawer>

                    </Hidden>


                    <Hidden smDown>
                        <StyledBox>
                            <nav>
                                <ul>
                                    {tabs.map((tab, index) => (
                                        <li key={`layout-tab-${index}`}>
                                            <Link to={tab.path}>{tab.icon} <span
                                                style={titleTabStyles}>{`${tab.title ? tab.title : ""}`}</span></Link>
                                        </li>
                                    ))}
                                </ul>
                            </nav>
                            <Button sx={buttonTabStyles} onClick={filterBySearch} disabled={error!==""}>Search</Button>
                            <IInput name="name" textFieldProps={{placeholder: "Search"}} padding="0" error={error}
                                    inputChangeHandler={inputChangeHandler}/>
                        </StyledBox>
                    </Hidden>
                </Grid>
                <Hidden smUp><Grid item xs={12} mt={'15px'} display={'flex'} flexDirection={'row-reverse'} justifyContent={'space-between'}
                                   alignItems={'baseline'}>
                    <Button sx={buttonTabStyles} onClick={filterBySearch} disabled={error!==""}>Search</Button>
                    <IInput name="name" textFieldProps={{placeholder: "Search"}} padding="0" error={error}
                            inputChangeHandler={inputChangeHandler}/>
                </Grid>
                </Hidden>
            </Grid>
        </Box>
    );

}


const logoStyles = {
    width: "5em",
    height: "2em"

};
const ulMenuMobile = {
   flexDirection:"column",
    padding:"15px 30px"

};


const titleTabStyles = {
    fontSize: 14,
    color: "rgba(96, 93, 236, 1)",
    marginLeft: 10,
    textAlign:'left',
    textWrap:'nowrap'
};

const buttonTabStyles = {
    minWidth: 100,
    height: 42,
    fontSize: 14,
    color: "rgba(255, 255, 255, 0.87)",
    backgroundColor: `rgba(96, 93, 236, 1)`,
    marginLeft: "10px",
    textTransform: 'none',
    "&:hover":{
        color: "rgba(255, 255, 255, 0.87)",
        backgroundColor: `rgba(96, 93, 236, 0.8)`,
    },
    "&:disabled":{
        color: "rgba(255, 255, 255, 1)",
        backgroundColor: `rgba(96, 93, 236, 0.5)`,
    }

};
export {LayoutTabs};
