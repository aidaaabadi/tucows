
import styled from "@emotion/styled";
import {  Box } from "@mui/material";

export const StyledBox = styled(Box)`
	display: flex;
	flex-direction:row-reverse;
	
    ul {
        display: flex;
        gap: 32px;
        margin: 0
    }
    li {
        display: flex;
        list-style-type: none;
        
    }
    li a {
       font-family: interFont;
        display: flex;
        align-items: center;
        height: 42px;
        border-top: 3px solid transparent;
        border-bottom: 3px solid transparent;
        text-decoration: none;
        color: ${(props) => props.theme.palette.secondary.main};
        font-weight: 400;
        transition: border-bottom-color 0.3s;
        &:hover {
            border-bottom-color: ${(props) => props.theme.palette.primary.main};
        }
    } 
`;
