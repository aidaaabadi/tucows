import {TableCell, TableRow, Typography, Hidden} from "@mui/material";
import {IStatusBadge} from "@/features/ui/i-status-badge";
import { PRODUCT_STATUS } from "@/utils/enums";
import Checkbox from '@mui/material/Checkbox';
function ProductListItem({ product, onShow,multiSelection }) {
	return (
		<>
		<Hidden smDown>
		<TableRow >
			<TableCell> {multiSelection && < Checkbox />}{product.id}</TableCell>
			<TableCell>
				<IStatusBadge status={PRODUCT_STATUS[product.status]} color={product.status} />
			</TableCell>
			<TableCell>{product.quantity || "---"}</TableCell>
			<TableCell onClick={onShow} style={{cursor:"pointer"}}>
				<Typography fontSize="14px">
					{product.name || "---"}	</Typography>
					<Typography color={"secondary.300"} fontSize="12px">{product.serial || "---"}</Typography>

			</TableCell>
			<TableCell>{`$${product.total}` || "---"}</TableCell>
		</TableRow>
		</Hidden>

	<Hidden smUp>
		<TableRow onClick={onShow} style={{cursor:"pointer"}}>

			<TableCell>
				<Typography textAlign={'left'} fontSize="14px">
					{product.name || "---"}	</Typography>
				<Typography color={"secondary.300"} textAlign={'left'} fontSize="12px">{`${product.serial || "---"} - Qty:${product.quantity}` }</Typography>
			</TableCell>
		</TableRow>
	</Hidden>
		</>
	);
}

export { ProductListItem };
