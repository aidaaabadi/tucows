import {Button, Grid, ListItemIcon, Typography} from "@mui/material";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import CircleIcon from '@mui/icons-material/Circle';
// Components
import {IDialog} from "@/features/ui/i-dialog";


const ProductDetail = ({open, loading, onClose, product, isMobile}) => {
    const closeHandler = () => {
        if (loading) return;
        onClose();
    };
    return (
        <IDialog open={open} loading={loading} strip size="sm" isMobile={isMobile} onClose={closeHandler}
                 onClick={(e) => e.stopPropagation()} title={product?.name}>

            <Grid container alignItems={'self-start'} flexDirection={'row'} width={'95%'} m={"15px auto"}>


                <Grid item xs={12} sm={4} md={4} lg={6} display={'flex'} justifyContent={'center'} mt={isMobile ? "-50px" : 0}>
                    <img src={product?.img}/>
                </Grid>
                <Grid item xs={12} sm={8} md={8} lg={6} mt={'20px'}>
                    <Typography fontSize="12px" fontWeight={'bold'} color={"secondary.500"}>Key Features</Typography>
                    <List>
                        {product?.features.map((item,index) => (
                            <ListItem sx={wrapperListStyles} key={index} >
                                <ListItemIcon sx={listStylesCircle}>
                                    <CircleIcon fontSize={'2px'} width={'0.5em'}/>
                                </ListItemIcon>
                                <Typography fontSize="12px" color={"secondary.500"}>{item}</Typography>
                            </ListItem>
                        ))}

                    </List>

                    <Typography fontSize="12px" mt={"15px"} color={"secondary.500"}
                                lineHeight={"25px"}>{product?.description}</Typography>

                </Grid>


                <Grid item xs={12} sm={12} md={12} lg={12} mt={'60px'} textAlign={'right'}>
                    <Button
                        color="secondary"
                        size="small"
                        sx={{
                            width: isMobile ? "100%" : "90px",
                            textTransform: "none",
                            backgroundColor: "secondary.100"
                        }}
                        disabled={loading}
                        onClick={onClose}
                    >
                        Close
                    </Button>

                </Grid>
            </Grid>
        </IDialog>
    );
};
const wrapperListStyles = {
    justifyContent: "flex-start",
    paddingBottom: 0,
    paddingLeft: 0,
    fontSize: 12
};

const listStylesCircle = {
    minWidth: 0,
    fontSize: 6,
    marginRight: "5px",
    color: "secondary.500"

};

export {ProductDetail};
