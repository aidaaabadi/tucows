import { useState } from "react";
import { Box } from "@mui/material";
// Assets
import { StyledTextField } from "./i-input.styles";
import InputAdornment from "@mui/material/InputAdornment";
import SearchIcon from '@mui/icons-material/Search';
function IInput({padding,textFieldProps, inputProps,inputChangeHandler,error }) {

	const [inputType, setInputType] = useState(textFieldProps?.type ? textFieldProps.type : "text");

	let paddingValue = "20px";
	if (!!padding) paddingValue = padding;
	else if (textFieldProps?.helperText) paddingValue = "30px";
	return (
		<Box
			sx={{
				position: "relative",
				display: "grid",
				pb: paddingValue,
			}}
		>
			<StyledTextField
				variant="outlined"
				inputProps={inputProps}
				error={Boolean(error)}
				helperText={error}
				type={inputType}
				onChange={inputChangeHandler}
				InputProps={{
					startAdornment: (
						<InputAdornment position="start">
							<SearchIcon />
						</InputAdornment>
					),
				}}
				{...textFieldProps}
			/>

		</Box>
	);
}

export { IInput };
