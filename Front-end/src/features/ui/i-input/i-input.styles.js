import styled from "@emotion/styled";
import {TextField} from "@mui/material";

export const StyledInputPasswordBox = styled.div`
	position: absolute;
	top: 12px;
	/* @noflip */
	right: 20px;
	display: flex;
	cursor: pointer;
`;
export const StyledTextField = styled(TextField)`
	& > div {
		/* @noflip */
		border-radius:0px 4px 4px 0px;
		color: ${(props) => props.theme.palette.secondary["600"]} !important;
		// width:240px;
		
	textarea {
		padding: 0 10px;
	}
	svg{
	color: ${(props) => props.theme.palette.secondary["600"]} !important;
	}
	fieldset{
	border-color:${(props) => props.theme.palette.secondary["600"]} !important;
		}
		input {
			/* @noflip */
			
			padding-left: 2px;
		color: ${(props) => props.theme.palette.secondary["400"]} !important;	
		}	
	legend {
			opacity: 0;
			// display:none
			
		}
			label {
			transform: translate(16px, -8px) scale(1) !important;
			font-size: 14px !important;
			color: ${(props) => props.theme.palette.secondary["600"]} !important;
			background-color: #fff;
			padding: 0 4px;
		}
	
	&.isPassword {




	}
	&.clearable {
		input {
			padding-right: 40px;
		}
	}
`;
