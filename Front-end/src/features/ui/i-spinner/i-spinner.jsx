import { Grid, CircularProgress } from "@mui/material";

function ISpinner() {
	return (
		<Grid item xs={12} sx={{ display: "flex", justifyContent: "center", my: "2rem" }}>
			<CircularProgress />
		</Grid>
	);
}

export { ISpinner };
