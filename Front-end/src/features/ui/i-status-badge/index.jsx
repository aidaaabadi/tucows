import { Typography } from "@mui/material";

// Assets
import { StyledBox } from "./i-status-badge.styles";

function IStatusBadge({ status, color }) {
	const colors = {
		1: {
			bg: "green.100",
			text: "green.main",
		},
		2: {
			bg: "orange.100",
			text: "orange.main",
		},
		3: {
			bg: "red.100",
			text: "red.main",
		},
		4: {
			bg: "primary.100",
			text: "primary.main",
		},
	};

	return (
		<StyledBox bgcolor={colors[color].bg}>
			<Typography color={colors[color].text} fontSize="12px" fontWeight={500}>
				{status}
			</Typography>
		</StyledBox>
	);
}

export { IStatusBadge };
