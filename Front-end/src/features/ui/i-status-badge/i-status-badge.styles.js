import styled from "@emotion/styled";
import { Box } from "@mui/material";

export const StyledBox = styled(Box)`
	border-radius: 12px;
	height: 30px;
	width: 83px;
	display: flex;
	align-items: center;
	justify-content: center;
	padding: 4px 10px;
`;
