import {Dialog, DialogContent, DialogTitle, IconButton, Typography} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

const IDialog = ({ children, open, onClose, size = "md" ,title,isMobile}) => {
	return (
		<Dialog
			open={open}
			onClose={onClose}
			scroll={"body"}
			fullWidth={true}
			maxWidth={size}
			PaperProps={{
				sx: {
					borderRadius: "8px",
					padding: "15px 10px",
					boxShadow:"none",
					margin:isMobile ? 0 : 32,
					// marginTop:isMobile ? "100px" : "32px",
					marginTop: "32px",
					minWidth:isMobile ? "100%" : "auto"

				},
			}}
			slotProps={{
				backdrop: {
					sx: {
						// backgroundColor: isMobile ? "transparent":"rgba(0, 0, 0, 0.4)",
						backgroundColor: "rgba(0, 0, 0, 0.4)",
					},
				},
			}}
		>
			<DialogTitle >
				<Typography fontSize="20px" color="secondary.main" fontFamily={"interFont"} fontWeight={"700"}>
					<strong >{title}</strong>
				</Typography>
			</DialogTitle>
			<IconButton
				aria-label="close"
				onClick={onClose}
				sx={{
					position: 'absolute',
					right: isMobile ? 4:23,
					top: 26,
					color: (theme) => theme.palette.secondary[400],
				}}
			>
				<CloseIcon />
			</IconButton>



			<DialogContent sx={{ padding: 0 }}>
				 {children} 
			</DialogContent>
		</Dialog>
	);
};

export { IDialog };
