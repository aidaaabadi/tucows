import {TableContainer, TableHead, TableRow, TableCell, Typography} from "@mui/material";

// Assets
import { StyledTable } from "./i-table.styles";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
function ITable({ children }) {
	return (
		<TableContainer>
			<StyledTable className="d-table">{children}</StyledTable>
		</TableContainer>
	);
}

function Head({ columns,isMobile }) {
	return (
		<TableHead>
			<TableRow>
				{columns.map(({ title, attrs = {} }, index) => (
					<TableCell key={index} {...attrs} >
						{index===(columns.length-1) && !isMobile ?
							<Typography display={'flex'} fontWeight={'700'} justifyContent={'space-between'} textAlign={'center'}>{title} < KeyboardArrowDownIcon /></Typography>
							:
						title

						}

					</TableCell>
				))}
			</TableRow>
		</TableHead>
	);
}

ITable.Head = Head;
export { ITable };
