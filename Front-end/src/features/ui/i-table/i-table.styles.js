import styled from "@emotion/styled";
import { Table } from "@mui/material";

export const StyledTable = styled(Table)`
	border-spacing: 0;
	border-collapse: separate !important;
	border-radius: 8px;
	border: 1px solid rgba(228, 228, 239, 1);
	overflow: hidden;

     td:last-child {
     border-left: 1px solid rgba(228, 228, 239, 1);
     text-align:right
     }
     
    td{
 	  font-size:14px;
 	  padding:8px;
     }
     
   th {
     font-size:16px;
     font-weight:700;

 }     
   th:last-child {
     font-size:16px;
     font-weight:700;
     text-align:center;
 }

`;
 