import { Pagination, Grid } from "@mui/material";
// Assets
import { StyledBox } from "./i-pagination.styles";
function IPagination({ page, count, onChange,limit }) {
	const totalPages = Math.ceil(count / limit);

	return (
		<Grid item xs={12}>
			{
				<StyledBox display="grid">
					<Pagination page={page} count={totalPages} showFirstButton showLastButton onChange={onChange} />
				</StyledBox>
			}
		</Grid>
	);
}

export { IPagination };
