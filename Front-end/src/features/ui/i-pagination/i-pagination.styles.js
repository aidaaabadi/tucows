import styled from "@emotion/styled";
import { Box } from "@mui/material";

export const StyledBox = styled(Box)`
	margin-top: 40px;
	display: flex;
	justify-content: center;
	padding-top: 24px;
	// border-top: 1px solid ${(props) => props.theme.palette.secondary["400"]};
`;
