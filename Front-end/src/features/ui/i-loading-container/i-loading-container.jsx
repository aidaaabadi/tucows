import {Box, Grid} from "@mui/material";

function ILoadingContainer({loading, children}) {
    return (
        <Grid container sx={{position: "relative"}}>
            {loading && (
                <Box
                    sx={{
                        position: "absolute",
                        top: 0,
                        right: 0,
                        width: "100%",
                        height: "100%",
                        backgroundColor: "rgba(255,255,255, 0.7)",
                        zIndex: 1,
                    }}
                />
            )}

            {children}
        </Grid>
    );
}

export {ILoadingContainer};
