 
const makeQueryString = (query) => {
	if (!query) return {};
	let temp = {};
	Object.keys(query).forEach((paramKey) => {
		if (!!query[paramKey] || typeof query[paramKey] === "number") temp[paramKey] = query[paramKey];
	});
	return new URLSearchParams(temp);
};
export { makeQueryString };
