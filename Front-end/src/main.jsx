import React from "react";
import ReactDOM from "react-dom/client";
import { muiTheme } from "@/config/mui-theme";
import { CssBaseline, GlobalStyles } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import toast, { Toaster } from "react-hot-toast";
import { SWRConfig } from "swr";

import { AppRoutes } from "@/routing/app-routes";

ReactDOM.createRoot(document.getElementById("root")).render(

		<ThemeProvider theme={muiTheme}>
			<CssBaseline />
				<GlobalStyles
					styles={{
						body: { backgroundColor: "#FFFFFF" },
					}}
				/>
				{/* global swr config */}
				<SWRConfig
					value={{
						keepPreviousData: true, // keep the data on pagination. with the default behaviour when we change the page swr will clear the data field
						revalidateOnFocus: false,
						dedupingInterval: 0,
						onError: (error, key) => {
							console.log(error, 987);
							if (error?.response?.status === 404) {
								toast.error("Something is wrong!");
							}
						},
					}}
				>
					<AppRoutes />
				</SWRConfig>
				<Toaster
					toastOptions={{
						style: {
							fontSize: "14px",
						},
					}}
				/>

		</ThemeProvider>
);
