# React.js + Vite Project README

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Getting Started

### Prerequisites

Before running the React.js project, ensure that the backend is up and running. Follow the steps below to set up:

1. Start the backend server by navigating to [http://localhost:8000](http://localhost:8000) in your web browser. Confirm that you receive the "Root Backend" text to verify the backend is functioning correctly.

### Installation

To install the necessary dependencies for the React.js project, run the following commands:


### `npm install`

### `npm run dev`

Runs the app in the development mode.\
Open [http://localhost:5173](http://localhost:5173) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

2.If you want to have multi select in table, change multiSelection={true} in src->screen->products.jsx (line 76)
3.If you want to have pagination, change limit: any number, in src->screen->products.jsx (line 34)